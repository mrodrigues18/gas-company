class Delivery {
  final int? idDelivery;
  final String start;
  final String destination;
  final String end;
  final int distance;
  final String state;

  Delivery(
      {
      this.idDelivery,
      required this.start,
      required this.destination,
      required this.end,
      required this.distance,
      required this.state});

  Delivery.fromMap(Map<String, dynamic> res) :
        idDelivery = res['idDelivery'],
        start = res['start'],
        destination = res['destination'],
        end = res['end'],
        distance = res['distance'],
        state = res['state'];

  Map<String, Object?> toMap() {
    return {
      'idDelivery':idDelivery,
      'start':start,
      'destination':destination,
      'end':end,
      'distance':distance,
      'state':state
    };
  }
}