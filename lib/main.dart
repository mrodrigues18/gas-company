import 'package:flutter/material.dart';
import 'package:gascompany/fragments/deliveries_pages/list_deliveries.dart';
import 'package:gascompany/fragments/history_pages/history.dart';
import 'package:gascompany/fragments/maps_pages/maps.dart';
import 'package:gascompany/splash_screen.dart';
import 'package:motion_tab_bar/MotionTabBarView.dart';
import 'package:motion_tab_bar/MotionTabController.dart';
import 'package:motion_tab_bar/motiontabbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Init.instance.initialize(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            /// Ajout du splash screen
            return MaterialApp(
                debugShowCheckedModeBanner: false,
                home: Splash()
            );
          } else {
            return MaterialApp(
              /// Cacher la barre de mode "Debug"
              debugShowCheckedModeBanner: false,
              title: 'The Los Santos State Gas Company',
              theme: ThemeData(
                primarySwatch: Colors.brown,
              ),
              home: MyHomePage(title: 'TLSSGC'),
            );
          }
        }
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  late MotionTabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = MotionTabController(initialIndex: 1, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: MotionTabBar(
        labels: [
          "Carte", "Le groupe", "Livraisons"
        ],
        initialSelectedTab: "Le groupe",
        tabIconColor: Colors.brown,
        tabSelectedColor: Color.fromRGBO(236, 216, 35, 1.0),
        onTabItemSelected: (int value) {
          setState(() {
            _tabController.index = value;
          });
        },
        icons: [
          Icons.map, Icons.text_snippet, Icons.car_rental
        ],
        textStyle: TextStyle(color: Colors.brown),
      ),
      body: MotionTabBarView(
        controller: _tabController,
        children: <Widget> [
          Container(
            child: MapsPage(assetSvg: 'assets/deliveries/gtavmap.svg'),
          ),
          Container(
            child: Center(
              child: HistoryPage(),
            ),
          ),
          Container(
            child: Center(
              child: DeliveriesListPage(),
            ),
          ),
        ],
      )
    );
  }
}
