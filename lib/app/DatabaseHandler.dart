
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:gascompany/modele/delivery.dart';

class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, 'gascompany.db'),
      onCreate: (database, version) async {
          await database.execute(
            "CREATE TABLE deliveries(idDelivery INTEGER PRIMARY KEY AUTOINCREMENT, start TEXT NOT NULL, destination TEXT, end TEXT NOT NULL, distance INTEGER NOT NULL, state TEXT NOT NULL)",
          );
      },
      version: 1
    );
  }

  Future<int> insertDelivery(List<Delivery> deliveries) async {
    int result = 0;
    final Database db = await initializeDB();
    for(var delivery in deliveries) {
      result = await db.insert('deliveries', delivery.toMap());
    }
    return result;
  }

  Future<List<Delivery>> retrieveDeliveries() async {
    final Database db = await initializeDB();
    final List<Map<String, Object?>> queryResult = await db.query('deliveries');
    return queryResult.map((e) => Delivery.fromMap(e)).toList();
  }

  Future<List<Delivery>> retrieveDeliveriesStates() async {
    final Database db = await initializeDB();
    final List<Map<String, Object?>> queryResult = await db.rawQuery('SELECT DISTINCT state FROM "deliveries"');
    return queryResult.map((e) => Delivery.fromMap(e)).toList();
  }

  Future<List<Delivery>> retrieveDeliveriesByState(List<String>? state) async {
    final Database db = await initializeDB();
    switch (state!.length) {
      case 1:
        final List<Map<String, Object?>> queryResult = await db.rawQuery('SELECT * FROM "deliveries" WHERE state IN("${state[0]}")');
        return queryResult.map((e) => Delivery.fromMap(e)).toList();
      case 2:
        final List<Map<String, Object?>> queryResult = await db.rawQuery('SELECT * FROM "deliveries" WHERE state IN("${state[0]}", "${state[1]}")');
        return queryResult.map((e) => Delivery.fromMap(e)).toList();
      case 3:
        final List<Map<String, Object?>> queryResult = await db.rawQuery('SELECT * FROM "deliveries" WHERE state IN("${state[0]}", "${state[1]}", "${state[2]}")');
        return queryResult.map((e) => Delivery.fromMap(e)).toList();
    }
    final List<Map<String, Object?>> queryResult = await db.rawQuery('SELECT * FROM "deliveries" WHERE state IN("${state[0]}")');
    return queryResult.map((e) => Delivery.fromMap(e)).toList();
  }

  Future<void> deleteDelivery(int idDelivery) async {
    final db = await initializeDB();
    await db.delete(
      'deliveries',
      where: "idDelivery = ?",
      whereArgs: [idDelivery],
    );
  }

  Future<void> erasePreviousData() async {
    final Database db = await initializeDB();
    await db.rawQuery("DELETE FROM sqlite_sequence WHERE name='deliveries'");
    await db.rawQuery("DELETE FROM deliveries");
  }
}