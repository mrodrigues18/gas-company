import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class HistoryPage extends StatefulWidget {
  HistoryPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => History();
}

class History extends State<HistoryPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: ListView(
        children: [
          Container(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/logo.png',
                      width: MediaQuery.of(context).size.width/5,
                      height: MediaQuery.of(context).size.height/10,
                    ),
                    Text("À propos de nous".toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: MediaQuery.of(context).size.width/20,
                      ),
                      textAlign: TextAlign.end,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Text(
                        "Premier groupe de compagnie énergétique sur l'île. " +
                            "Employant des centaines de personnes et comptant plusieurs dizaines de collaborateurs professionnels, "
                                "nous utilisons des technologies simples et peu coûteuses en énergie pour favoriser un avenir énergétique durable. "
                                "A l'écart de la ville, nous mettons tout en place pour assurer un confort optimal à nos employés tout en respectant la qualité de vie des habitants du quartier.",
                        style: TextStyle(
                            fontSize: 16.0
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Text(
                        "Nous livrons chaque jour des centaines de litres de pétroles que ce soit auprès des stations services, ou des compagnies aériennes ou maritimes.",
                        style: TextStyle(
                            fontSize: 16.0
                        ),
                      ),
                    ],
                  ),
                ),
                Image.asset('assets/gascompany.png'),
                Container(
                  padding: EdgeInsets.all(8.0),
                  color: Colors.yellow,
                  child: ListTile(
                    leading: Image.asset(
                      'assets/yves_barbet_selfie.png',
                    ),
                    title: Text(
                      "Yves BARBET - CEO",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 21.0
                      ),
                    ),
                    subtitle: Text(
                      "Yves Barbet est le Président de la Société des Pétroles de Los Santos (« The Los Santos State Gas Company »)",
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 14.0
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    onTap: () => _showPresidentDescription(context),
                    onLongPress: () => _showPhoneNumber(context),
                  ),
                ),
                Image.asset('assets/tanks.jpg')
              ],
            ),
          ),
        ],
      )
    );
  }

  _showPhoneNumber(context) {
    Alert(
      context: context,
      title: 'Numéro de téléphone:',
      desc: '(555)-7402704',
      buttons: [
        DialogButton(
          child: Text(
          "OK",
            style: TextStyle(
                color: Colors.white,
                fontSize: 20
            ),
          ),
          onPressed: () => Navigator.pop(context),
        )
      ]
    ).show();
  }
  
  _showPresidentDescription(context) {
    Alert(
      context: context,
      title: 'Yves BARBET - CEO',
      desc: "Homme élancé qui vient d'entrer dans la fleur de l'âge, mesurant 1m80 pour 85 kilogrammes, il est toujours muni de ses lunettes de soleil qu'il garde de jour comme de nuit. Il possède également un veston du LSFD. Yves est de nature discrète et garde toujours sur le visage un sourire éclatant.",
      image: Image.asset('assets/yves_barbet_selfie.png'),
      buttons: [
        DialogButton(
            child: Text(
              "OK",
              style: TextStyle(
                color: Colors.white, fontSize: 20
              ),
            ),
            onPressed: () => Navigator.pop(context),
        )
      ]
    ).show();
  }
}