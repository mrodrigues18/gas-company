import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MapsPage extends StatefulWidget {
  MapsPage({Key? key, required this.assetSvg}) : super(key: key);

  final String assetSvg;

  @override
  State<StatefulWidget> createState() => Maps();
}

class Maps extends State<MapsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: InteractiveViewer(
        child: Container(
          child: SvgPicture.asset(this.widget.assetSvg),
        ),
      )
    );
  }
}