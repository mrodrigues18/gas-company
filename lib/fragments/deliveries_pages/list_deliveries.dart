import 'package:filter_list/filter_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:gascompany/app/DatabaseHandler.dart';
import 'package:gascompany/fragments/maps_pages/maps.dart';
import 'package:gascompany/modele/delivery.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DeliveriesListPage extends StatefulWidget {
  DeliveriesListPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => DeliveriesList();
}

class DeliveriesList extends State<DeliveriesListPage> {
  late DatabaseHandler handler;

  List<Delivery> listOfDeliveries = [];
  List<String>? selectedCountList = [];
  List<String> listOfStates = [];

  @override
  void initState() {
    super.initState();
    this.handler = DatabaseHandler();
    this.handler.initializeDB().whenComplete(() async {
      await this.handler.erasePreviousData();
      await this.addDeliveries();
      this.countStates();
      setState(() {});
    });
  }

  Future<List<Delivery>> listToDisplay(List<String>? state) {
    if (selectedCountList!.length >=1 && state!.length >= 1) return this.handler.retrieveDeliveriesByState(state);
    else return this.handler.retrieveDeliveries();
  }

  List<String> countStates() {
    Future<List<Delivery>> result = this.handler.retrieveDeliveriesStates();
    result.then((value) {
      value.forEach((item) {
        listOfStates.add(item.state);
      });
      return listOfStates;
    });
    return [];
  }

  void _openFilterDialog() async {
    await FilterListDialog.display<String>(
        context,
        applyButonTextBackgroundColor: Colors.brown,
        selectedTextBackgroundColor: Colors.brown,
        closeIconColor: Colors.redAccent,
        listData: listOfStates,
        selectedListData: selectedCountList,
        height: 480,
        headlineText: "Selectionnez les villes",
        searchFieldHintText: "Nom d'une ville...",
        allButtonText: "Toutes",
        applyButtonText: "OK",
        resetButtonText: "Réinitialiser",
        choiceChipLabel: (item) {
          return item;
        },
        validateSelectedItem: (list, val) {
          return list!.contains(val);
        },
        onItemSearch: (list, text) {
          if (list!.any((element) =>
              element.toLowerCase().contains(text.toLowerCase()))) {
            return list
                .where((element) =>
                element.toLowerCase().contains(text.toLowerCase())).toList();
          } else {
            return [];
          }
        },
        onApplyButtonClick: (list) {
          if (list != null) {
            setState(() {
              selectedCountList = List.from(list);
            });
          }
          Navigator.pop(context);
        }
    );
  }

  Future<int> addDeliveries() async {
      Delivery d0 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service de Davis", distance: 1000, state: "Los Santos");
      Delivery d1 = new Delivery(start: "Murrieta Oil Field", destination: "Raffinerie", end: "Station-service de l'héliport", distance: 1000, state: "Los Santos");
      Delivery d2 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service de Mirror Park", distance: 1000, state: "Los Santos");
      Delivery d3 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service d'Hawick", distance: 2500, state: "Los Santos");
      Delivery d4 = new Delivery(start: "Murrieta Oil Field", destination: "Raffinerie", end: "Station-service de l'Aéroport de Los Santos", distance: 3000, state: "Los Santos");

      Delivery d5 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service de Sandy Shores", distance: 5000, state: "Sandy Shores");
      Delivery d6 = new Delivery(start: "Murrieta Oil Field", destination: "Raffinerie", end: "Station-service de Harmony", distance: 5000, state: "Sandy Shores");
      Delivery d7 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service de la Senora Freeway", distance: 5000, state: "Sandy Shores");
      Delivery d8 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service de Grapeseed", distance: 5000, state: "Sandy Shores");
      Delivery d9 = new Delivery(start: "Murrieta Oil Field", destination: "Raffinerie", end: "Station-service Aéroport de Sandy Shores", distance: 5000, state: "Sandy Shores");

      Delivery d10 = new Delivery(start: "Plateforme pétrolière en haute mer", destination: "", end: "Station-service de Paleto Bay", distance: 12000, state: "Paleto Bay");
      Delivery d11 = new Delivery(start: "Plateforme pétrolière en haute mer", destination: "", end: "Station-service de North Chumash", distance: 12000, state: "Paleto Bay");
      Delivery d12 = new Delivery(start: "Plateforme pétrolière en haute mer", destination: "", end: "Station-service de Chumash", distance: 12000, state: "Paleto Bay");
      Delivery d13 = new Delivery(start: "Murrieta Oil Field", destination: "", end: "Station-service du Fort Zancudo", distance: 10000, state: "Paleto Bay");


      listOfDeliveries = [d0, d1, d2, d3, d4,
                          d5, d6, d7, d8, d9,
                          d10, d11, d12, d13];
      return await this.handler.insertDelivery(listOfDeliveries);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        onPressed: _openFilterDialog,
        tooltip: 'Filtrer les résultats',
        child: Icon(Icons.filter_list),
      ),
      body: FutureBuilder(
        future: listToDisplay(selectedCountList!),
        builder: (BuildContext context, AsyncSnapshot<List<Delivery>> snapshot){
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Dismissible(
                    direction: DismissDirection.endToStart,
                    background: Container(
                      color: Colors.red,
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Icon(Icons.delete_forever),
                    ),
                    key: ValueKey<int>(snapshot.data![index].idDelivery!),
                    onDismissed: (DismissDirection direction) async {
                      await this.handler.deleteDelivery(snapshot.data![index].idDelivery!);
                      setState(() {
                        snapshot.data!.remove(snapshot.data![index]);
                      });
                    },
                    child: Column(
                      children: [
                        Card(
                          child: CupertinoButton(
                            child: ListTile(
                              contentPadding: EdgeInsets.all(8.0),
                              leading: Text(snapshot.data![index].state),
                              title: Text(snapshot.data![index].start),
                              subtitle: Text(snapshot.data![index].distance.toString() + "m"),
                            ),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) {
                                        return new MapsPage(assetSvg: 'assets/deliveries/gtavmap_d${snapshot.data![index].idDelivery}.svg');
                                      },
                                    fullscreenDialog: true,
                                  )
                              );
                            },
                          )
                        ),
                      ],
                    )
                  );
                }
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}